<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhraseData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $phrase = new Phrase();
        $phrase->translate('ru')->setText('Привет мир');
        $phrase->translate('en')->setText('Hello world');
        $phrase->translate('de')->setText('Hallo Welt');
        $phrase->translate('fr')->setText('Bonjour le monde');
        $phrase->translate('es')->setText('Hola mundo');

        $manager->persist($phrase);
        $phrase->mergeNewTranslations();

        $phrase = new Phrase();
        $phrase->translate('ru')->setText('Жизнь прекрасна');
        $phrase->translate('en')->setText('Life is beautiful');
        $phrase->translate('de')->setText('Das Leben ist schön');
        $phrase->translate('fr')->setText('La vie est belle');
        $phrase->translate('es')->setText('La vida es hermosa');

        $manager->persist($phrase);

        $phrase->mergeNewTranslations();
        $manager->flush();

    }

}
