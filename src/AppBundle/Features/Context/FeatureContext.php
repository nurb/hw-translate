<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;


    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     *
     */
    public function __construct()
    {

    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function iSeeWordOnAPage($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я перехожу на страницу авторизации/
     */
    public function goToLogInPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('fos_user_security_login'));
    }


    /**
     * @When /^я авторизуюсь с логином "([^"]*)" и паролем "([^"]*)"/
     */
    public function userIsAuthenticatingItselfWith($login, $password)
    {
        $this->fillField('username', $login);
        $this->fillField('password', $password);
        $this->pressButton('_submit');
    }

    /**
     * @When /^я заполняю форму со фразой "([^"]*)"/
     */
    public function iFeelFieldWithPhrase($phrase)
    {
        $this->fillField('form_addPhrase', $phrase);
        $this->pressButton('form_save');
    }

    /**
     * @When /^я заполняю перевод фраз/
     */
    public function iFeelFieldWithTranslate()
    {
        $this->fillField('form_en', "Test Phrase in English");
        $this->fillField('form_de',"Test Phrase In German");
        $this->fillField('form_fr',"Test Phrase In French");
        $this->fillField('form_es',"Test Phrase In Spain");
        $this->pressButton('form_save');
    }

    /**
     * @When /^я жму на ссылку "([^"]*)"/
     */
    public function iAmLookingForLink($phrase)
    {
        $link = $this->getSession()->getPage()->findLink($phrase);
        $link->click();
    }


    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function iAmOnMainPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }


    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }
}
