<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PhrasesController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /**
         * @var $phrases Phrase
         */
        $phrases = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->findAll();
        if ($this->getUser()) {
            $form = $this->createFormBuilder()
                ->add('addPhrase', TextType::class, ['label' => $this->get('translator')->trans('Place order for translation')])
                ->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Save'), 'attr' => array(
                    'class' => 'btn btn-success',
                )])
                ->getForm();

            $form->handleRequest($request);
            $formView = $form->createView();

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $newPhrase = new Phrase();
                $newPhrase->translate('ru')->setText($data['addPhrase']);
                $em = $this->getDoctrine()->getManager();
                $em->persist($newPhrase);
                $newPhrase->mergeNewTranslations();
                $em->flush();

                return $this->redirectToRoute('homepage');
            }
        } else {
            $formView = null;
        }

        return $this->render('@App/Phrases/index.html.twig', array(
            'phrases' => $phrases,
            'form' => $formView
        ));
    }

    /**
     * @Route("/phrase/{id}", name="edit_phrase")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {
        $phrase = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->findOneBy(['id' => $id]);

        if ($this->getUser()) {

            /**
             * @var $english PhraseTranslation
             */
            $english = $phrase->translate('en', false);
            /**
             * @var $german PhraseTranslation
             */
            $german = $phrase->translate('de', false);
            /**
             * @var $french PhraseTranslation
             */
            $french = $phrase->translate('fr', false);
            /**
             * @var $spanish PhraseTranslation
             */
            $spanish = $phrase->translate('es', false);

            $form = $this->createFormBuilder();
            if ($english->isEmpty()) {
                $form->add('en', TextType::class, ['label' => $this->get('translator')->trans('English translation'), 'required' => false]);
            }
            if ($german->isEmpty()) {
                $form->add('de', TextType::class, ['label' => $this->get('translator')->trans('German translation'), 'required' => false]);
            }
            if ($french->isEmpty()) {
                $form->add('fr', TextType::class, ['label' => $this->get('translator')->trans('French translation'), 'required' => false]);
            }
            if ($spanish->isEmpty()) {
                $form->add('es', TextType::class, ['label' => $this->get('translator')->trans('Spanish translation'), 'required' => false]);
            }
            if (in_array(true, [$english->isEmpty(), $german->isEmpty(), $french->isEmpty(), $spanish->isEmpty()])) {
                $form->add('save', SubmitType::class, ['label' => $this->get('translator')->trans('Save'), 'attr' => array(
                    'class' => 'btn btn-success',
                )]);
            }

            $form = $form->getForm();
            $form->handleRequest($request);
            $formView = $form->createView();

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                if ($form->has('en') && $data['en'] != null) {
                    $english->setText($data['en']);
                } else {
                    $phrase->getNewTranslations()->remove('en');
                }
                if ($form->has('de') && $data['de'] != null) {
                    $german->setText($data['de']);
                } else {
                    $phrase->getNewTranslations()->remove('de');
                }
                if ($form->has('fr') && $data['fr'] != null) {
                    $french->setText($data['fr']);
                } else {
                    $phrase->getNewTranslations()->remove('fr');
                }
                if ($form->has('es') && $data['es'] != null) {
                    $spanish->setText($data['es']);
                } else {
                    $phrase->getNewTranslations()->remove('es');
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($phrase);

                $phrase->mergeNewTranslations();
                $em->flush();
                return $this->redirect($request->headers->get('referer'));
            }
        } else {
            $formView = null;
        }


        return $this->render('@App/Phrases/edit.html.twig', array(
            'phrase' => $phrase,
            'form' => $formView
        ));
    }

}
