<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class LanguageController extends Controller
{
    /**
     * @Route("language/{language}", name="setLanguage", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @param $language
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setLanguage(Request $request,$language)
    {
        $this->get('session')->set('_locale', $language);
        return $this->redirect($request->headers->get('referer'));
    }


}
